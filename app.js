// Iteracion #1 - Buscar el máximo

function sum(numberOne, numberTwo) {
    numberOne > numberTwo ? numberOne : numberTwo;
}

// Iteracion #2 - Buscar la palabra más larga

const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(avengers) {
    let pLarga = '';
    avengers.forEach(avenger => {
        if (avenger.length > pLarga.length) {
            pLarga = avenger;
        }
    });
    return pLarga;
}

// Iteracion #3 - Calcular la suma

const numbers1 = [1, 2, 3, 5, 45, 37, 58];
function sumAll(nums) {
    let total = 0;
    nums.forEach(num => {
        total += num;
    });
    return total;
}

// Iteracion #4 - Calcular promedio

const numbers2 = [12, 21, 38, 5, 45, 37, 6];
function average(nums) {
    let total = 0;
    nums.forEach(num => {
        total += num;
    });
    return total / nums.length;
}

// Iteracion #5 - Calcular promedio de strings

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
function averageWord(params) {
    let total = 0;
    params.forEach(param => {
        if (typeof param === 'number') {
            total += param;
        } else {
            let sToNum = parseInt(param.length);
            total += sToNum;
        }
    });
    return total;
}

// Iteracion #6 - Valores únicos

const duplicates = ['sushi', 'pizza', 'burger', 'potatoe', 'pasta', 'ice-cream', 'pizza', 'chicken', 'onion rings', 'pasta', 'soda'];
function removeDuplicates(params) {
    let result = [];
    params.forEach(param => {
        if (result.includes(param) === false) {
            result.push(param);
        }
    });
    return result;
}

// Iteracion #7 - Buscador de nombres

const nameFinder = ['Peter', 'Steve', 'Tony', 'Natasha', 'Clint', 'Logan', 'Xabier', 'Bruce', 'Peggy', 'Jessica', 'Marc'];
function finderName(param1, param2) {
    let result = param1.find(param1 => param1.includes(param2));
    if (typeof result === 'string') {
        console.log(`${result} was found in the array.`);
    } else if (typeof result === 'undefined') {
        console.log(`Name not found in the array.`);
    }
}

// Iteracion #8 - Buscador de nombres

const counterWords = ['code', 'repeat', 'eat', 'sleep', 'code', 'enjoy', 'sleep', 'code', 'enjoy', 'upgrade', 'code'];
function repeatCounter(param) {
    let words = {};
    param.forEach(x => {
        words[x] = (words[x] || 0) + 1;
    });
    console.log(words);
}
